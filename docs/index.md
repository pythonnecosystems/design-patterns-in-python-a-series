# Python 디자인 패턴: A Series  <sup>[1](#footnote_1)</sup>

> *이 포스팅은 Python의 다양한 디자인 패턴의 복잡성에 대해 알아보는 흥미로운 시리즈의 시작을 알리는 포스팅이다*.

<a name="footnote_1">1</a>: [Design Patterns in Python: Decorator](https://medium.com/@amirm.lavasani/design-patterns-in-python-decorator-c882c0db6501)를 편역한 것이다.
